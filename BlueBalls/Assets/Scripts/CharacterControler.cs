﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterControler : MonoBehaviour
{
    public float speed;
    public Transform spawnPoint;
    
    public enum Direction
    {
        NONE = 0,
        UP,
        LEFT,
        DOWN,
        RIGHT
    } 
    
    private Rigidbody2D m_Rigidbody;
    private Animator m_Animator;

    private Vector2 m_speedVec;
    private Direction m_direction = Direction.NONE;

    public Vector2 GetSpeed() {
        return m_speedVec;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        transform.position = spawnPoint.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.UpdatePosition();
        this.UpdateAnim();
    }

    private void UpdatePosition()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");


        m_speedVec = new Vector2(horizontalInput, verticalInput);

        if(m_speedVec.magnitude > 1.0)
            m_speedVec.Normalize();

        m_Rigidbody.velocity = m_speedVec * speed;
    }

    private void UpdateAnim()
    {
        Direction direction = Direction.NONE;

        if(m_speedVec.magnitude > 0.01)
        {
            if(Math.Abs(m_speedVec.x) > Math.Abs(m_speedVec.y))
            {
                if(m_speedVec.x > 0)
                    direction = Direction.RIGHT;
                else
                    direction = Direction.LEFT; 
            }
            else
            {
                if(m_speedVec.y > 0)
                    direction = Direction.UP;
                else
                    direction = Direction.DOWN; 
            }
        }

        m_Animator.SetInteger("Direction", (int)direction);
        m_Animator.SetBool("Changed", m_direction != direction);
        m_direction = direction;
    }
}
