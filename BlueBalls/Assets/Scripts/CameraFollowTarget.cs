using System;
using UnityEngine;


public class CameraFollowTarget : MonoBehaviour
{
    public Transform target;
    public Vector3 offset = new Vector3(0f, 0f, 0f);


    private void LateUpdate()
    {
        transform.position = target.position + offset;
    }
}
